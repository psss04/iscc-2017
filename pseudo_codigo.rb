carrega_biblioteca(Open3)
classe Container
	construtor(nome)
		@nome = nome
		@limite_memoria = 0
		@limite_download = 0
		@limite_upload = 0
	fimconstrutor
	metodo pega_uso_memoria
		memoria_usada, erro_comando, status_comando = Open3.captura_comando("comando que pega a quantidade de memória usada pelo container")
		retorna converter_para_float(memoria_usada)
	fimmetodo
	metodo pega_memoria_total_host
		memoria_total_do_host, erro_comando, status_comando = Open3.captura_comando("comando que pega a quantidade total de memória do host")
		retorna converter_para_float(memoria_total_do_host)
	fimmetodo
	metodo estatisticas_rede
		largura_de_banda_usada, erro_comando, status_comando = Open3.captura_comando("comando que pega informações de uso de banda do container")
		retorna array(largura_de_banda_usada.velocidade_download, largura_de_banda_usada.velocidade_upload)
	fimmetodo
	metodo pega_uso_download
		retorna estatisticas_rede.primeiro_item
	fimmetodo
	metodo pega_uso_upload
		retorna estatisticas_rede.ultimo_item
	fimmetodo
	metodo media_harmonica(array_itens)
		soma = 0
		laço array_itens.percorre_cada_item
			soma += (1 / item_atual)
		fimlaço
		retorna array_items.quantidade_de_itens / soma
	fimmetodo
	metodo limita_memoria
		array_uso_memoria = []
		laço Ruby.executa_10_vezes # Laço da linguagem Ruby que executa o código dentro dele 10 vezes
			array_uso_memoria.adicionar_item(pega_uso_memoria)
		fimlaço
		media_consumo_memoria = media_harmonica(array_uso_memoria)
		se media_consumo_memoria >= ((pega_memoria_total_host * 90) / 100) # se media_consumo_memoria >= 90% da memória total do host
			@limite_memoria = pega_memoria_total_host
		senão
			@limite_memoria = ((media_consumo_energia * 110) / 100) @limite_memoria = 110% de media_consumo_energia
		fimse
		executa_comando_sistema(comando que seta o limite de memória para @limite_memoria)
	fimmetodo
	metodo limita_rede
		taxas_uso_de_upload = []
		taxas_uso_de_download = []
		laço Ruby.executa_10_vezes # Laço da linguagem Ruby que executa o código dentro dele 10 vezes
			taxas_uso_de_download.adicionar_item(pega_uso_download)
			taxas_uso_de_upload.adicionar_item(pega_uso_upload)
		fimlaço
		media_uso_download = media_harmonica(taxas_uso_download)
		media_uso_upload = media_harmonica(taxas_uso_upload)
		@limite_download = ((media_uso_download * 110) / 100) seta @limite_download para 110% do uso de download atual do container
		@limite_upload = ((media_uso_upload * 110) / 100) seta @limite_upload para 110% do uso de upload atual do container
		se @limite_download < 20
			@limite_download = 20
		fimse
		se @limite_upload < 20
			@limite_upload = 20
		fimse
		executa_comando_sistema(comando que seta o limite de download do container para @limite_download)
		executa_comando_sistema(comando que seta o limite de upload do container para @limite_upload)
	fimmetodo
fimclasse
container_um = Container.criar("nome_container")
container_um.limita_memoria
# container_um.limita_rede