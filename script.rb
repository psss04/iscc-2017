require 'open3'
require 'benchmark'
include Benchmark
class String
	def remove_lines(i)
		split("\n")[i..-1].join("\n")
	end
end
class Container
	# Talvez futuramente pegar o ID do container também
	attr_accessor :name, :memory_usage_limit, :download_usage_limit, :upload_usage_limit
	def initialize name
		@name = name
		@memory_usage_limit = 0
		@download_usage_limit = 0
		@upload_usage_limit = 0
	end
	def get_memory_usage
		used_memory, error, command_status = Open3.capture3("sudo lxc exec #{@name} -- free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 3")
		return used_memory.to_i
	end
	def get_device_total_memory
		total_memory, error, command_status = Open3.capture3("free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 2")
		return total_memory.to_f
	end
	def network_stats
		used_bandwidth, error, command_status = Open3.capture3("sudo lxc exec #{@name} -- ifstat -i eth0 -q 1 1")
		used_bandwidth = used_bandwidth.remove_lines(2).gsub!(" ", "\n")
		network_usage_stats = []
		used_bandwidth.each_line { |line| network_usage_stats << line.to_f if line != "\n" }
		return network_usage_stats
	end
	def get_download_usage
		return network_stats.first
	end
	def get_upload_usage
		return network_stats.last
	end
	def weighted_mean values
		return (values[0] * 4 + values[1] * 6 + values[2] * 10 + values[3] * 30 + values[4] * 50) / 5
	end
	def manage_memory_limit
		memory_usage_amounts = []
		5.times { memory_usage_amounts << get_memory_usage }
		memory_usage_mean = weighted_mean memory_usage_amounts
		if memory_usage_mean >= ((get_device_total_memory * 90) / 100)
			@memory_usage_limit = get_device_total_memory
		else
			@memory_usage_limit = (memory_usage_mean * 110) / 100
		end
		# puts "\n\n+---+---+---+ Gerenciamento de Memória +---+---+---+\n\n"
		system "sudo lxc config set #{@name} limits.memory #{@memory_usage_limit}MB"
		# puts "Total de memória: #{get_device_total_memory}\nMemória Usada: #{memory_usage_mean}\nLimite de memória: #{@memory_usage_limit}\n\n"
	end
	def manage_network_limits
		upload_rates = []
		download_rates = []
		5.times do
			download_rates << get_download_usage
			upload_rates << get_upload_usage
		end
		download_usage_mean = weighted_mean download_rates
		upload_usage_mean = weighted_mean upload_rates
		@download_usage_limit = ((download_usage_mean * 110) / 100) * 10
		@upload_usage_limit = ((upload_usage_mean * 110) / 100) * 10
		@download_usage_limit = 20 if @download_usage_limit < 20
		@upload_usage_limit = 20 if @upload_usage_limit < 20
		@download_usage_limit = @download_usage_limit.to_i
		@upload_usage_limit = @upload_usage_limit.to_i
		# puts "\n\n+---+---+---+ Gerenciamento de Rede +---+---+---+\n\n"
		system "sudo lxc profile device set default eth0 limits.ingress #{@download_usage_limit.to_i}kbit"
		system "sudo lxc profile device set default eth0 limits.egress #{@upload_usage_limit.to_i}kbit"
		# puts "Limite de Download: #{@download_usage_limit.to_i}\nUso de Download: #{download_usage_mean}"
		# puts "Limite de Upload: #{@upload_usage_limit.to_i}\nUso de Upload: #{upload_usage_mean}"
	end
end
# system 'sudo apt install ifstat'
container_one = Container.new 'ubuntu'
Benchmark.benchmark(CAPTION) do |benchmark|
	memory_management = benchmark.report("RAM: ") { container_one.manage_memory_limit }
	benchmark.report("Net: ") { container_one.manage_network_limits }
end
